# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 22:08:30 2018

@author: szyma

This file uses the function geo.stations_within_radious to build a list of stations
within 10 km of the Cambridge city centre (52.2053, 0.1218)
"""
# Import modules
from floodsystem import geo
from floodsystem import stationdata

# Build a list of all stations
station_list = stationdata.build_station_list()
# Get the list of stations within 10km from city centre
entries = geo.stations_within_radius(station_list, (52.2053, 0.1218), 10.0)
# create a list of names
list_names = []
for station_full_object in entries:
    # Add the name of the station
    list_names.append(station_full_object.name)
# Sort the list in alphabetical order
list_names.sort()
# And print it
print(list_names)