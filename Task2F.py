#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 13:37:31 2018

@author: shantan-ya
"""

'''
Task 2F

For each of the 5 stations at which the current relative water level is greatest 
and for a time period extending back 2 days, plots the level data and the best-fit 
polynomial of degree 4 against time. Show the typical range low/high on your plot.
'''

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from datetime import timedelta, datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels

#call new functions
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit




def plot_with_fit_2days4deg(station):
    
    num_days=2

    dates, levels=fetch_measure_levels(measure_id=station.measure_id, dt=timedelta(days=num_days)) 
    p=4
    
    plot_water_level_with_fit(station, dates, levels, p) 

# Build list of stations
station_list = build_station_list()

print('Task 2F. Plot with fit for 5 stations with current highest levels')

# Update latest level data for all stations
update_water_levels(station_list)


#remove stations if no current level info
for s in station_list:
    if type(s.latest_level)!= float:
        station_list.remove(s)
#second time to get all stations (as change of list length means it doesn't work first time) 
for s in station_list:
    if s.latest_level!= float:
        station_list.remove(s)
    
#sort stations by water level, from highest to lowest   

sorted_by_level = sorted(station_list, key=lambda x: x.latest_level, reverse=True)

plot_with_fit_2days4deg(station=sorted_by_level[0])
plot_with_fit_2days4deg(station=sorted_by_level[1])
plot_with_fit_2days4deg(station=sorted_by_level[2])
plot_with_fit_2days4deg(station=sorted_by_level[3])
plot_with_fit_2days4deg(station=sorted_by_level[4])