


'''
Task 2G

Using your implementation, list the towns where you assess the risk of flooding 
to be greatest. Explain the criteria that you have used in making your assessment,
 and rate the risk at ‘severe’, ‘high’, ‘moderate’ or ‘low’.
'''


import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from datetime import timedelta, datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations 
from floodsystem.stationdata import build_station_list, update_water_levels

#call new functions
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit

#call functions from categories
from floodsystem.categories import consistency
from floodsystem.categories import Marker1
from floodsystem.categories import gradient
from floodsystem.categories import Marker2



#making some functions
def categories(stations, num_days):   
    print('The following stations have inconsistent data:')
    print('')
    consistency(stations)
    
    marker1, notmarker1 =Marker1(stations)
    marker2, notmarker2=Marker2(stations, num_days) #takes about 5 minutes to run
    
    
    severe=[x for x in marker1 if x in marker2]
    high=[y for y in marker1 if y in notmarker2]
    moderate = [z for z in notmarker1 if z in marker2]
    low = [a for a in notmarker1 if a in notmarker2]
    return severe, high, moderate, low

def plot_with_fit_4deg(station, num_days):

    dates, levels=fetch_measure_levels(measure_id=station.measure_id, dt=timedelta(days=num_days)) 
    p=4
    
    plot_water_level_with_fit(station, dates, levels, p) 
    return




print('Task 2G')
print('')

station_list=build_station_list()
num_days=5
severe, high, moderate, low = categories(station_list, num_days)

print('The stations are discarded if the data is inconsistent. They are then sorted into 4 categories')
print('Severe: {} stations \nHigh: {} stations \nModerate: {} stations \nLow: {} stations'.format(len(severe), len(high), len(moderate), len(low)))      
print('The names of the stations most at risk of flooding, along with an example graph')
severe_stations=[b.name for b in severe]
print(severe_stations)

print('A graph to show a typical severe risk station')
plot_with_fit_4deg(severe[0], num_days)
print('A graph to show a typical high risk station')
plot_with_fit_4deg(high[0], num_days)
print('A graph to show a typical moderate risk station')
plot_with_fit_4deg(moderate[0], num_days)
print('A graph to show a typical low risk station')
plot_with_fit_4deg(low[0], num_days)

