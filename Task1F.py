# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 16:45:24 2018

@author: szyma
Returns a list of names of staitons with inconsistent water levels
"""

from floodsystem import station
from floodsystem import stationdata

# Build a list of all stations
station_list = stationdata.build_station_list()
# Create a list of stations with inconsistent ranges
inconsistent_stations = station.inconsistent_typical_range_stations(station_list)
# Create a lsit of names
inconsistent_stations_names = []
for station_entry in inconsistent_stations:
    inconsistent_stations_names.append(station_entry.name)
# Sort in alphabetical order
inconsistent_stations_names.sort()
print (inconsistent_stations_names)