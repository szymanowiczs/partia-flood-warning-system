# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 15:49:42 2018

@author: szyma

This file is for testing the funcitons in the module flood
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem import flood
def test_stations_level_over_threshold():
        
    stations_list = build_station_list()
    
    update_water_levels(stations_list)
    test_tol = 0.4
    stations_in_danger = flood.stations_level_over_threshold(stations_list, test_tol)
    
    for instance_station_level in stations_in_danger:
        assert instance_station_level[1]>test_tol

def test_stations_highest_rel_level():
    
    stations_list = build_station_list()
    
    update_water_levels(stations_list)
    test_N = 10
    high_risk_stations = flood.stations_highest_rel_level(stations_list, test_N)
    
    assert len(high_risk_stations) == test_N