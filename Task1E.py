#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 27 17:20:45 2018

@author: shantan-ya
"""

"""
Task 1E function for geo
Implement a function in geo that determines the N rivers with the greatest number of monitoring stations. 
It should return a list of (river name, number of stations) tuples, sorted by the number of stations.
 In the case that there are more rivers with the same number of stations as the N th entry, 
 include these rivers in the list. The function should have the signature:
def rivers_by_station_number(stations, N):
"""


from floodsystem.stationdata import build_station_list
from floodsystem import station
from floodsystem import stationdata

from floodsystem.geo import rivers_by_station_number


#prints 9 rivers with greatest number of stations
stations = build_station_list()

print(rivers_by_station_number(stations, 9))