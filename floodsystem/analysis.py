#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 13:47:15 2018

@author: shantan-ya


This is a submodule for analyis

"""
import matplotlib
import numpy as np


'''
This function, given dates, water levels and a polynomial degree, returns a poly1d type array of
least squares best fit polynomial coefficients
'''
def polyfit(dates, levels, p):
    
    #convert dates to floats
    dates = matplotlib.dates.date2num(dates)
    
    #choose the first date as a start-point, to make calculations more accurate (smaller numbers)
    d0 = dates[0]
    
    # Using shifted x values, find coefficient of best-fit
    p_coeff=np.polyfit(dates-d0, levels, p)
    
    #poly1d type
    poly = np.poly1d(p_coeff)
    
    return (poly, d0)