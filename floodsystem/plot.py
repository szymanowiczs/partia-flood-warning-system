# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 15:25:43 2018

@author: szyma

This is a submodule for plotting
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.analysis import polyfit

 

"""
Plot_water_levels is a function that displays a plot, using matplotlib, of the 
water level against time for a station, and includes on the plot lines for the 
typical low and high levels. The axes are labelled and use the station name as 
the plot title.
"""

    
def plot_water_levels(station, dates, levels):
    if type(station)!= list:
        #plot levels against time
        plt.plot(dates, levels, label='live data')
        
        #plotting typical low and high levels
        plt.plot(dates,[station.typical_range[0]] * len(dates), label='typical low level')
        plt.plot(dates,[station.typical_range[1]] * len(dates), label='typical high level')
        
        #with legend
        plt.legend()
        
        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('date')
        plt.ylabel('water level (m)')
        plt.xticks(rotation=45);
        plt.title(station.name)
        
        # Display plot
        plt.tight_layout()  # This makes sure plot does not cut off date labels
        plt.show()
        return 
    elif type(station)==list:

        n=len(station)
        if n>6:
            raise ValueError('This function takes up to 6 stations only')
        
        else:
            for o in range(1,n+1):
                plt.figure(num=None, figsize=(7, 9), dpi=90, facecolor='w', edgecolor='r')
                
                plt.subplot(n,1,o)
                
                #plot levels against time
                plt.plot(dates[o-1], levels[o-1], label='live data')
                
                #plotting typical low and high levels
                plt.plot(dates[o-1],[station[o-1].typical_range[0]] * len(dates[o-1]), label='typical low level')
                plt.plot(dates[o-1],[station[o-1].typical_range[1]] * len(dates[o-1]), label='typical high level')
                
                #with legend
                plt.legend()
                
                # Add axis labels, rotate date labels and add plot title
                plt.xlabel('date')
                plt.ylabel('water level (m)')
                plt.xticks(rotation=45);
                plt.title(station[o-1].name)
                plt.tight_layout()     
                
                plt.show()
            return
'''
plot_water_level_with_fit is a function that displays a plot, using matplotlib, of the 
water level against time for a station,and the least squares fit line of a chosen 
degree of polynomial.

'''

def plot_water_level_with_fit(station, dates, levels, p):
    
    #call polyfit to get coeffs and x-axis shift
    poly_tuple=polyfit(dates, levels, p)
    
    #extract poly and d0
    poly=poly_tuple[0]
    d0=poly_tuple[1]
    
    #convert dates to floats
    dates = matplotlib.dates.date2num(dates)
    
    # Plot polynomial fit at n points along interval
    n=100 #arbitrary choice
    
    x1 = np.linspace(dates[0], dates[-1], n)
    plt.plot(x1, poly(x1-d0), label ='least squares fit') #using shifted dates value
    
    #plot 'real' levels against time
    plt.plot(dates, levels, label='live data')
    
    #plotting typical low and high levels
    plt.plot(dates,[station.typical_range[1]] * len(dates), label='typical high level')
    plt.plot(dates,[station.typical_range[0]] * len(dates), label='typical low level')
    
    #with legend
    plt.legend()
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    
    plt.show()