#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 19:58:20 2018

@author: shantan-ya

These functions were written for Task2G to sort the stations into groups based on risk of flooding

"""


import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from datetime import timedelta, datetime
from floodsystem.stationdata import update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations 
from floodsystem.stationdata import build_station_list, update_water_levels

def consistency(stations):
    #This checks and removes stations from a given list which have no data for latest water level or typical range
  
    # Update latest level data for all stations
    update_water_levels(stations)
    
    #remove stations if no current level info
    for s in stations:
        if type(s.latest_level)!= float:
            stations.remove(s)
            print('{} has been removed, lack of data about latest level'.format(s.name))
        else:
            pass
    for t in stations:
        if type(t.latest_level)!= float:
            stations.remove(t)
            print('{} has been removed, lack of data about latest level'.format(t.name)) 
            
    #remove station if no typical range info
    for k in stations:
        if type(k.typical_range)!= tuple:
            stations.remove(k)
            print('{} has been removed, lack of data about typical range'.format(k.name))
    for u in stations:
        if type(u.typical_range)!= tuple:
            stations.remove(u)
            print('{} has been removed, lack of data about typical range'.format(u.name))
    stations=[x for x in stations if x not in inconsistent_typical_range_stations(stations)]
            
    return stations

    
def Marker1(stations):
    #This takes a list of stations and puts in into 2 groups. marker1 contains stations where the latest level is above average
    #notmarker1 contains stations where latest level is below average
    marker1=[]
    notmarker1=[]
    for station in stations:

        if station.latest_level>((station.typical_range[1]-station.typical_range[0])/2)+station.typical_range[0]:                   
            marker1.append(station)
        else:
            notmarker1.append(station)
            
        
    return  marker1, notmarker1



def gradient(station, num_days):
    #This takes one stations and draws a polyfit curve for the past num_days days. It calculates the latest
    #gradient and if it is positive, returns True
    
    p=4
    #find gradient at latest time for one station
    try:
        dates, levels=fetch_measure_levels(measure_id=station.measure_id, dt=timedelta(days=num_days))
        dates = matplotlib.dates.date2num(dates)
        try:
            d0=dates[-1]
            
            #get polynomial fit for each station
            p_coeff=np.poly1d(np.polyfit(dates-d0, levels, p))
            diff=p_coeff.deriv()
            grad=diff(dates[0]-d0)
            if grad>0:
                return True
        except (IndexError, ValueError):
            print(station.name, 'returns an IndexError or ValueError')
            pass
                 
            
    except KeyError:
        print(station.name, 'returns a KeyError')
        pass
    return None
    
        
 
def Marker2(stations, num_days):
    #This sorts stations into 2 groups. marker2 contains stations where latest gradient is positive.
    #notmarker2 contains stations where latest gradient is not positive
    marker2=[]
    notmarker2=[]
    for station in stations:
        
        if gradient(station, num_days)==True:
            marker2.append(station)
        else:
            notmarker2.append(station)
    return marker2, notmarker2



