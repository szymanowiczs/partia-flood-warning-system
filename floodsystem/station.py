"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None


    def __repr__(self):
        d =  "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    """
    This method checks if the data for the typical range is consistent, that is
    it exists and the low level is lower than the high level
    """
    def typical_range_consistent(self):
        # Checks if data exists at all
        if not isinstance(self.typical_range, tuple) or len(self.typical_range)<2:
            return False
        # Checks if the reported typical low level is lower than the high level
        elif self.typical_range[0]>self.typical_range[1]:
            return False
        else:
            return True
    
    """
    This method returns the latest water level as a fraction of the typical
    range, i.e. a ratio of 1.0 corresponds to a level at they typucal high and
    a ratio of 0.0 corresponds to a level at the typical low. Returns none if
    the necessary data is not available or is inconsistent.
    """
    def relative_water_level(self):
        # Checks if the needed data is avaliable and consistent
        if not self.typical_range_consistent() or self.latest_level==None:
            return None
        else:
            # Finds the range
            difference = self.typical_range[1] - self.typical_range[0]
            relative_level = self.latest_level-self.typical_range[0]
            # Calculates the level as a fraction
            fraction = relative_level/difference
            return fraction
            
"""
Function that, given a list of station objects, returns a list of stations that 
have inconsistent data. The function should use typica_range_consistent.
"""    
def inconsistent_typical_range_stations(stations):
    inconsistent_stations = []
    # For each station checks if the range is consistent using the method from the 
    # class above
    for station in stations:
        if not station.typical_range_consistent():
            inconsistent_stations.append(station)
    return inconsistent_stations

