"""This module contains a collection of functions related to
geographical data.
"""


from haversine import haversine
# deleted the dot before utils because otherwise said __main__.utils is not a module


from .utils import sorted_by_key


from floodsystem.stationdata import build_station_list

#from .utils import sorted_by_key


from floodsystem import station
from floodsystem import stationdata




"""
This function, given a list of stations and a coordinate p, returns
a list of (station, distance) tuples, where distance is the distance
of the station from the coordinate p. The returned list should be
sorted by distance. 
"""

def stations_by_distance(stations_input, p):
    # Stations is a list of objects MonitoringStation
    # P is a tuple of floats for the coordinate p
    # distance is a float, distance from a station to p
    # Initialise the list for the stations
    # Iterate through the list of stations
    listStations = []
    # for all stations in the list the function calculates distance
    # and adds the tuple to the list
    for station in stations_input:
        coordinates_station = station.coord
        # Calculate actual distance between two points on a sphere
        distance_station = haversine(coordinates_station, p)
        pair = station, distance_station
        listStations.append(pair)
    # sort the stations by distance
    listStations = sorted_by_key(listStations, 1)
    return listStations


"""
This function returns a list of all stations within radius r of a geographic
coordinate x
"""

def stations_within_radius(stations, centre, r):
    stations_within = []
    # Sort the stations in increasing distance from the centre
    sort_by_dist = stations_by_distance(stations, centre)
    i = 0
    # And keep adding to the list of stations within radius until too far
    while sort_by_dist[i][1]<r:
        stations_within.append(sort_by_dist[i][0])
        i+=1
    return stations_within
    
'''
This function, given a list of stations, returns a set of rivers linked to those stations.
'''



def rivers_with_station(stations):
    
    river_list=[station.river for station in stations]
        
    river_set=set(river_list)
    return river_set



'''
This function, given a list of stations, returns a dictionary with a set of rivers as the keys, and lists of the stations
on those rivers as the object. It also calls the function rivers_with_stations.
'''

def stations_by_river(stations):
    
    river_list=[station.river for station in stations]
    stations_list=[station.name for station in stations]
    
    #a new list of lists associating rivers and stations
    new_list=[]
    for i in range(len(stations_list)):
        new_list.append(0)
        new_list[i]=[river_list[i],stations_list[i]]
    
    #call rivers_with_station and make the set into a list to enable iteration
    river_set_list=list(rivers_with_station(stations))
    
    #iterate through river_set_list and make a new list based on this set which groups all stations corresponding to a river
    new_set_list=[]
    for river in range(len(river_set_list)):
        new_set_list.append([river_set_list[river]])
        for h in range(len(new_list)):
            if new_list[h][0]==river_set_list[river]:
                new_set_list[river].append(new_list[h][1])
    
    #making a dictionary out of lists
    keys=[element[0] for element in new_set_list]
    objects=[element[1:] for element in new_set_list]
    
    dictionary=dict(zip(keys, objects))
    return(dictionary)


"""
This function, for a given list of stations and a number of values N, 
returns a list of the N rivers with the greatest number of monitoring stations. 
It calls rivers_with_station() in geo.
"""
def rivers_by_station_number(stations, N):
    
    #making a list of tuples of all rivers and their station numbers
    river_list=list(rivers_with_station(stations))
    river_dict=stations_by_river(stations)
    list_rivers=[]
    list_numbers=[]
    
    for h in range(len(river_list)):
        list_rivers.append(0)
        list_numbers.append(0)
        list_rivers[h] = river_list[h]
        list_numbers[h]=len(river_dict[river_list[h]])
    
    list_of_tuples=list(zip(list_rivers,list_numbers))
    
    #create a dictionary: {key=list_N, item=[list of tuples of same number of stations]}
    #find N_max using a set of number of stations
    set_of_numbers={i[1] for i in list_of_tuples}
    set_of_numbers=sorted(list(set_of_numbers))
    
    max_N=len(set_of_numbers)
    
    #This checks that N is valid
    if N>max_N or N<0 or type(N)!=int:
            raise ValueError('the value of N is incorrect')
    
    #list_N will be the keys
    list_N=[n for n in range(1,max_N+1)]
    list_N.reverse()
    
    #new_list will be items 
    new_list=[[l] for l in set_of_numbers]
    
    for k in range(len(list_of_tuples)):
        for j in range(len(set_of_numbers)):
            if list_of_tuples[k][1]==list(set_of_numbers)[j]:
                
                new_list[j].append(list_of_tuples[k])
    
    new_list_sliced = [sublist[1:] for sublist in new_list]
    
    #making dictionary
    dictionary=dict(zip(list_N,new_list_sliced))
    
    # now to return all rivers up to N
    N_top_rivers=[]    
    for k in range(N-1):
        N_top_rivers.append(dictionary[k+1]) 
        
    # Turning the list of lists into a flat list    
    N_top_rivers = [item for sublist in N_top_rivers for item in sublist]
    
    return(N_top_rivers)



"""
This function, given a list of stations, plots them on the map of the UK from
google maps using library bokeh. Produces an output html file. As input takes 
a list of stations and a centre to start displaying around
"""

def stations_on_map(stations, centre, name, colour_dots):
    from bokeh.io import output_file, show
    from bokeh.models import GMapPlot, GMapOptions, ColumnDataSource, Circle, Range1d, PanTool, WheelZoomTool, BoxSelectTool

    # Set the options of the map
    map_options = GMapOptions(lat=centre[0], lng=centre[1], map_type="terrain", zoom=11)
    plot = GMapPlot(x_range=Range1d(), y_range=Range1d(), map_options=map_options)
    plot.title.text = name
    # Get API - Google Maps requires this for the GMAP to work
    plot.api_key = "AIzaSyCpyIZsKCKXzDHBj5pjwVw-QbZXfGD1vN4"
    # create lists of latitudes and longitudes
    lat_list = []
    lon_list = []
    for station in stations:
        lat_list.append(station.coord[0])
        lon_list.append(station.coord[1])
    
    source = ColumnDataSource(
            data = dict(
                    lat = lat_list, 
                    lon = lon_list,
            )
    )
    
    circle = Circle(x="lon", y="lat", size = 15, fill_color=colour_dots, fill_alpha = 0.8, line_color=None)
    plot.add_glyph(source, circle)
    
    plot.add_tools(PanTool(), WheelZoomTool(), BoxSelectTool())
    output_file(name + ".html")
    show(plot)


