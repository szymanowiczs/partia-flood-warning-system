# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 16:06:05 2018

@author: szyma

This sumbodule is related to flooding

This function returns a list of tuples, where each tuple holds (1) a station at
which the latest relative level is over tol and (2) the relative water level at 
the station. The returned list should be sorted by the relative level in descending
order.Checks if the relative_water_level function returns a value before comparing
to tol.
"""
#from .station import MonitoringStation
from .utils import sorted_by_key
from .station import MonitoringStation
from .geo import stations_on_map

def stations_level_over_threshold(stations, tol):
    # Initialise the list of stations with higher relative water levels
    stations_danger = []
    for station_instance in stations:
        # If above the tolerant level add to the list of stations in danger
        relative_level = station_instance.relative_water_level()
        if relative_level!= None:
            if relative_level > tol:
                name_level = station_instance, relative_level
                stations_danger.append(name_level)
    # Sort in reverse by the relative level
    return sorted_by_key(stations_danger, 1, True)

"""
This function returns a list of the N stations at which the water level, 
relative to the typical range, is highest. The list should be sorted in 
descending order by relative level. 
"""
def stations_highest_rel_level(stations, N):
    # set a low tolerance to get a list of stations with relative levels in 
    # descending order
    low_level = -1.0e10
    all_stations = stations_level_over_threshold(stations, low_level)
    all_stations_cut = all_stations[:N]
    high_risk = []
    for station_level_instance in all_stations_cut:
            high_risk.append(station_level_instance[0])

    return high_risk


    