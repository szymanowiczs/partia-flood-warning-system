#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 00:11:31 2018

@author: shantan-ya
"""


'''
Implement in a submodule plot a function that displays a plot (using Matplotlib) of the water level 
data against time for a station, 
and include on the plot lines for the typical low and high levels. The axes should be labelled 
and use the station name as the plot title. The function should have the signature:
def plot_water_levels(station, dates, levels):
Option: In place of Matplotlib, try using a web-centric Python plotting library such as Bokeh orPlotly.
Optional extension: Generalise your implementation such that it takes a list of up to 6 stations displays the level at each station as subplot inside a single plot.
'''

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_levels


import matplotlib.pyplot as plt
from datetime import timedelta, datetime
import random



def plot_ten_days(station):
    dt=10
    dates, levels=fetch_measure_levels(measure_id=station.measure_id, dt=timedelta(days=dt))  
    return plot_water_levels(station, dates, levels)
            

# Build list of stations
station_list = build_station_list()


print('Task 2E. Plot for 5 stations with current highest levels')

# Update latest level data for all stations
update_water_levels(station_list)


#remove incosistent data      
for s in station_list:
    if s.latest_level==None:
        station_list.remove(s)
#second time to get all stations (as change of list length means it doesn't work first time) 
for s in station_list:
    if s.latest_level==None:
        station_list.remove(s)
      
        

#sort stations by water level, from highest to lowest   

sorted_by_level = sorted(station_list, key=lambda x: x.latest_level, reverse=True)


plot_ten_days(station=sorted_by_level[0])
plot_ten_days(station=sorted_by_level[1])
plot_ten_days(station=sorted_by_level[2])
plot_ten_days(station=sorted_by_level[3])
plot_ten_days(station=sorted_by_level[4])


#Task 2E extension. The function takes up to 6 stations and plots the data as subplots


print('Task 2E extension. The function takes up to 6 (random) stations and plots the data as subplots')

dt = timedelta(days=10)

stations=random.sample(station_list, 6)
#obtaining dates and levels for more than one station
if type(stations)==list:

    #make  a list of levels, corresponding to to station list
    levels_list=[fetch_measure_levels(l.measure_id, dt)[1] for l in stations]
    dates=[fetch_measure_levels(l.measure_id, dt)[0] for l in stations]

#obtaining dates and levels for just one station
elif type(stations)== MonitoringStation:
    dates, levels_list=fetch_measure_levels(stations.measure_id, dt)  
    
plot_water_levels(stations,dates, levels=levels_list)    
    

