"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation
from floodsystem import stationdata
from floodsystem.station import inconsistent_typical_range_stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    # Tries different types of erroneous ranges
    trange = [1, (), [2, 14], (2), (4, 2), (2, 14)]
    river = "River X"
    town = "My Town"
    s = []
    for i in range(len(trange)):
        s.append(MonitoringStation(s_id, m_id, label, coord, trange[i], river, town))
    # Checks that the incorrect ones are flagged as incorrect
    for i in range(len(trange)-1):
        assert s[i].typical_range_consistent() == False
    # And that the correct one is treated as a correct range
    assert s[-1].typical_range_consistent() == True
    
def test_inconsistent_typical_range_stations():
    # Build a list of all stations
    station_list = stationdata.build_station_list()
    inconsistent_range_stations = inconsistent_typical_range_stations(station_list)
    for station_erroneous in inconsistent_range_stations:
        assert station_erroneous.typical_range_consistent()==False

def test_relative_water_level():
  
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s.latest_level = 3.102

    assert s.relative_water_level() > 0.92
    assert s.relative_water_level() < 0.96    

