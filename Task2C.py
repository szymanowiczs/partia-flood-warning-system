# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 16:22:28 2018

@author: szyma

Prints the names of the 10 stations at which the current relative level is 
highest, with the relative level beside each station name.
"""

# Import modules
from floodsystem import flood
from floodsystem import stationdata

# Build a list of all stations
station_list = stationdata.build_station_list()
# Update water levels
stationdata.update_water_levels(station_list)

N = 10
top_N_stations = flood.stations_highest_rel_level(station_list, N)
for station_instance in top_N_stations:
    print (station_instance.name, station_instance.relative_water_level())
    