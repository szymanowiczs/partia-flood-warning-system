# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 12:41:04 2018

@author: szyma

This file contains tests for functions in module geo
in library floodwarning. 

Test for funtion stations_by_distance involves checking that the entries
have increasing distance from a given point (Cambridge city centre)

Test for function stations_within_distance involves checking that the function
returns stations which are actually within that radius.

"""
import pytest
# Module to be tested
from floodsystem import geo
# Module to be used in testing
from floodsystem import stationdata
import random
# Initialise the internal state
random.seed()
def test_stations_by_distance():
    # Build a list of all stations
    full_station_list = stationdata.build_station_list()
    # Sort them by distance from Cmabridge City Centre
    stations_from_Cam = geo.stations_by_distance(full_station_list, (52.2053, 0.1218))
    for i in range(1, len(stations_from_Cam)):
        # Checks that entries are sorted by distance
        assert stations_from_Cam[i][1]>=stations_from_Cam[i-1][1]

def test_stations_within_radius():
    # Build a list of all stations
    full_station_list = stationdata.build_station_list()
    # Distance to test, use random not to introduce bias
    distance_testing = random.random()*450
    # Get the results from the function
    stations_within_random_distance = geo.stations_within_radius(full_station_list, (52.2053, 0.1218), distance_testing)
    # Have a list of stations and their distance from the Cambridge City Centre
    stations_random_sorted = geo.stations_by_distance(stations_within_random_distance, (52.2053, 0.1218))
    # Check that the distance is smaller than required radius
    for station_with_distance in stations_random_sorted:
        assert station_with_distance[1]<=distance_testing  
        
            
def test_stations_by_river():
    full_station_list = stationdata.build_station_list()
    #pick 5 random stations
    selected_stations = random.sample(full_station_list, 5)    
    #check that the river each station is on acts as the key to the station itself using the stations_by_river function
    for station in selected_stations:
        assert station.name in geo.stations_by_river(selected_stations)[station.river]
        
def test_rivers_by_station_number():

    # Build a list of all stations
    full_station_list = stationdata.build_station_list()
    
    #Create random value for N
    N=random.randint(0,20)
    
    #check that only the N top rivers are shown by calculating the range of station numbers and equalling it to N
    returned_list=geo.rivers_by_station_number(full_station_list, N)
    set_of_station_numbers={i[1] for i in returned_list}
    assert len(set_of_station_numbers)==N
    
    #checking the returned tuples really are the top values:
    #by making a set of all station numbers
    river_list=list(geo.rivers_with_station(full_station_list))
    river_dict=geo.stations_by_river(full_station_list)
    list_rivers=[]
    list_numbers=[]
    
    for h in range(len(river_list)):
        list_rivers.append(0)
        list_numbers.append(0)
        list_rivers[h] = river_list[h]
        list_numbers[h]=len(river_dict[river_list[h]])
    all_station_numbers=set(list_numbers)
    
    #make 2 sets: one of top station numbers (returned by function), another of all station numbers. Compare
    top=set_of_station_numbers
    all=all_station_numbers
    
    rest=all-top
    
    for j in top:
        for k in rest:
            assert j>k

