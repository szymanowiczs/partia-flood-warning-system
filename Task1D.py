#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 11:01:06 2018

@author: shantan-ya

Task1D.py
"""


from floodsystem.stationdata import build_station_list
from floodsystem import station
from floodsystem import stationdata

from floodsystem.geo import rivers_with_station, stations_by_river


stations =  stationdata.build_station_list()

#print number of rivers with at least one station on them

rivers_with_station(stations)
print(len(rivers_with_station(stations)))


#sort by alphabetical order and print first 10 rivers
sorted_set=sorted(rivers_with_station(stations))
print(sorted_set[:10])


#for a given river, print the associated stations in alphabetical order
def sorted_stations_for_river(given_river):
    sorted_stations=sorted(stations_by_river(stations)[given_river])
    print(sorted_stations)
    
sorted_stations_for_river('River Aire')
sorted_stations_for_river('River Cam')
sorted_stations_for_river('Thames')