# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 16:18:15 2018

@author: szyma
This is a demonstration program for Task 1B and it prints the name of each station 
at which current relative level is over 0.8, with the relative level alongside
the name
"""
# Import modules
from floodsystem import flood
from floodsystem import stationdata

# Build a list of all stations
station_list = stationdata.build_station_list()
# Update water levels
stationdata.update_water_levels(station_list)
    

set_tol = 0.8
stations_endangered = flood.stations_level_over_threshold(station_list, set_tol)
for station_instance in stations_endangered:
    print(station_instance[0].name, station_instance[1])