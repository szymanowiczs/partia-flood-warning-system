# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 19:22:13 2018

@author: szyma

This demonstration program prints a list of tuples for the 10 closest and
the 10 furthest stations from the Cambridge city centre
"""
# Import modules
from floodsystem import geo
from floodsystem import stationdata

station_list = stationdata.build_station_list()
entries = geo.stations_by_distance(station_list, (52.2052, 0.1218))
closest_stations = []
furthest_stations = []
for full_station_entry in entries[:10]:
    station_info = full_station_entry[0].name, full_station_entry[0].town, full_station_entry[1]
    closest_stations.append(station_info)
for full_station_entry in entries[-10:]:
    station_info = full_station_entry[0].name, full_station_entry[0].town, full_station_entry[1]
    furthest_stations.append(station_info)

print("The 10 closest stations are: ", closest_stations, "\n")
print("The 10 furthest stations are: ", furthest_stations, "\n")